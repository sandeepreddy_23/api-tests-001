*** Settings ***
Library  RequestsLibrary//RequestsKeywords.py
Library  Collections
Library  String
Library  OperatingSystem
Library  customAuthenticator.py

*** Variables ***
${STATUS_CODE_200}  200
${GIT_URL}  https://api.github.com
${USER_ENDPOINT}  /user
${REPO_ENDPOINT}  /users/sandeepreddy237/repos
${ISSUE_ENDPOINT}  /repos/sandeepreddy237/myfirstrepo/issues
${BRANCH_ENDPOINT}  /repos/sandeepreddy237/myfirstrepo/branches

*** Keywords ***

Get git user details
    [Documentation]    Gets the data of the git user

    ${resp}=  Get Request  github  ${USER_ENDPOINT}
    Should Be Equal As Strings  ${resp.status_code}  ${STATUS_CODE_200}
    [return]  ${resp.json()}

Get git repo details
    [Documentation]    Gets the data of the git repo
    ${resp}=  Get Request  github  ${REPO_ENDPOINT}
    Should Be Equal As Strings  ${resp.status_code}  ${STATUS_CODE_200}
    [return]  ${resp.json()}

Get git issue details
    [Documentation]    Gets the data of the git issue
    ${resp}=  Get Request  github  ${ISSUE_ENDPOINT}
    Should Be Equal As Strings  ${resp.status_code}  ${STATUS_CODE_200}
    [return]  ${resp.json()}

Get data for google user
    [Documentation]    Gets the data of the google user
    ${resp}=  Get Request  google  /
    Should Be Equal As Strings  ${resp.status_code}  ${STATUS_CODE_200}

Get data
    [Documentation]    Gets the data from  json file
    ${jsonfile}    Get File  ../data/data.json
    ${json}=    evaluate  json.loads('''${jsonfile}''')    json
    set global variable  ${data}  ${json}
    [return]  ${json}

Get git branches details
    [Documentation]    Gets the data of the git branch
    ${resp}=  Get Request  github  ${BRANCH_ENDPOINT}
    Should Be Equal As Strings  ${resp.status_code}  ${STATUS_CODE_200}
    [return]  ${resp.json()}