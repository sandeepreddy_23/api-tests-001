*** Settings ***
Documentation   Sample tests for api testing
...             store api response in a variable
...             then verifies specific values

Resource  ../resource.robot
Suite Teardown  Delete All Sessions
Suite Setup  Get data

*** Test Cases ***

Get Requests to login and get user details
    [Documentation]    git authentication and gets user data

    ${headers}=  Create Dictionary  Authorization=token %{TOKEN}
    Create Session  github  ${GIT_URL}  headers=${headers}
    ${resp_body}=  Get git user details
    Should Be Equal As Strings  ${resp_body['type']}  ${data['git_user']['type']}
    Should Be Equal As Strings  ${resp_body['login']}  ${data['git_user']['login']}
    Should Be Equal As Strings  ${resp_body['id']}  ${data['git_user']['id']}

Get repo details
    [Documentation]    Calls git repo api and gets repo details

    ${resp_repo}=  Get git repo details
    Should Be Equal As Strings  ${resp_repo[0]['default_branch']}  ${data['git_repo']['default_branch']}
    Should Be Equal As Strings  ${resp_repo[0]['id']}  ${data['git_repo']['id']}
    Should Be Equal As Strings  ${resp_repo[0]['full_name']}  ${data['git_repo']['full_name']}

Get issue details
    [Documentation]     Cals git issue api and gets issue details

    ${resp_issue}=  Get git issue details
    Should Be Equal As Strings  ${resp_issue[0]['id']}  ${data['git_issue']['id']}
    Should Be Equal As Strings  ${resp_issue[0]['body']}  ${data['git_issue']['body']}
    Should Be Equal As Strings  ${resp_issue[0]['title']}  ${data['git_issue']['title']}

Get details of git branches
    [Documentation]    calls git branches api and gets branch details

    ${resp_branches}=  Get git issue details
    Should Be Equal As Strings  ${resp_branches[0]['name']}  ${data['git_branch']['name']}
    Should Be Equal As Strings  ${resp_branches[0]['protected']}  ${data['git_branch']['protected']}
    Should Be Equal As Strings  ${resp_branches[0]['protection_url']}  ${data['git_branch']['protection_url']}